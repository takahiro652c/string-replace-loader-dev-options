const path = require('path');
const outputPath = path.resolve(__dirname, 'output-dev');
const config = require('./webpack.config.shared');

module.exports = config(false, outputPath);
