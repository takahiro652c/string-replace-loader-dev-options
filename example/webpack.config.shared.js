const stringReplaceLoaderDevOptions = require('..');

module.exports = (isProd, path) => {
  return {
    mode: 'development',
    entry: './src/index.js',
    output: {
      filename: 'index.js',
      path
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [stringReplaceLoaderDevOptions.js({ isProd })]
        }
      ]
    }
  };
};
