const path = require('path');
const outputPath = path.resolve(__dirname, 'output-prod');
const config = require('./webpack.config.shared');

module.exports = config(true, outputPath);
