# string-replace-loader-dev-options

[![npm](https://img.shields.io/npm/v/string-replace-loader-dev-options.svg)](https://www.npmjs.com/package/string-replace-loader-dev-options)
[![npm downloads](https://img.shields.io/npm/dm/string-replace-loader-dev-options.svg)](https://www.npmjs.com/package/string-replace-loader-dev-options)
[![gitlab build](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/badges/master/build.svg)](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/commits/master)

Return [string-replace-loader's](https://github.com/Va1/string-replace-loader)
option which is useful for development.


*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/README.ja.md)*

## Installation

```bash
npm i -D string-replace-loader-dev-options
```

## Usage

If you want options for javascript, change your `webpack.config.js` like that:

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.js$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.js({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Available functions

* [js](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/js.md)
* [html](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/html.md)
* [sass](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/sass.md)



## License
MIT
