# string-replace-loader-dev-options

[![npm](https://img.shields.io/npm/v/string-replace-loader-dev-options.svg)](https://www.npmjs.com/package/string-replace-loader-dev-options)
[![npm downloads](https://img.shields.io/npm/dm/string-replace-loader-dev-options.svg)](https://www.npmjs.com/package/string-replace-loader-dev-options)
[![gitlab build](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/badges/master/build.svg)](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/commits/master)
[![gitlab coverage](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/badges/master/coverage.svg)](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/commits/master)

Webpackのloaderの1つである[string-replace-loader](https://github.com/Va1/string-replace-loader)
の開発時に便利なオプションを返す。

*Read this in other languages: [Engulish](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/README.md)*

## Installation

```bash
npm i -D string-replace-loader-dev-options
```

## Usage

js用のオプションを使う場合、`webpack.config.js`を次のようにしてください:

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.js$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.js({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Available functions

* [js](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/js.ja.md)
* [html](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/html.ja.md)
* [sass](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/sass.ja.md)


## License
MIT
