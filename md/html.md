# html

*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/html.ja.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.html({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (required)

### prefix: `string` (default: `'\\$\\$\\.'`)

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

## Replacements

### <!-- $$.onlyDevStart --> ... <!-- $$.onlyDevEnd -->
### <!-- $$.onlyDevStart ... $$.onlyDevEnd -->

```html
<!-- $$.onlyDevStart -->
<a href="./debug.html">debug page</a>
<!-- $$.onlyDevEnd -->
<!-- $$.onlyDevStart
<a href="./debug.html">debug page</a>
$$.onlyDevEnd -->
```

If `isProd` is **falsy**:

```html

<a href="./debug.html">debug page</a>


<a href="./debug.html">debug page</a>

```

If `isProd` is **truthy**:

```html
<!--
<a href="./debug.html">debug page</a>
-->
<!--
<a href="./debug.html">debug page</a>
-->
```

### <!-- $$.onlyProdStart --> ... <!-- $$.onlyProdEnd -->
### <!-- $$.onlyProdStart ... $$.onlyProdEnd -->

```html
<!-- $$.onlyProdStart -->
production mode
<!-- $$.onlyProdEnd -->
<!-- $$.onlyProdStart
production mode
$$.onlyProdEnd -->
```

If `isProd` is **falsy**:

```html
<!--
production mode
-->
<!--
production mode
-->
```

If `isProd` is **truthy**:

```html

production mode


production mode

```
