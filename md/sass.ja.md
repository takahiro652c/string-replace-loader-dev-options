# sass

*Read this in other languages: [English](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/sass.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.sass$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.sass({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (必須)

webpackがプロダクションモードかどうかを表す式を指定してください。

### prefix: `string` (default: `'\\$\\$\\.'`)

置換対象の文字列の接頭辞を指定します。

この文字列は RegExp() に渡されるため、エスケープが必要な文字が存在します。
(例: 接頭辞として`$`を使用したい場合は、`'\\$'`を指定してください。)

つまり、接頭辞のデフォルトは`$$.`です。
以降の説明ではデフォルトの接頭辞を想定します。

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

[string-replace-loader](https://github.com/Va1/string-replace-loader)
に渡すoptionsの配列を指定できます。

## Replacements

### // $$.onlyDev

```scss
body {
  color: gray; // $$.onlyDev
}
```

If `isProd` is **falsy**:

```scss
body {
  color: gray;
}
```

If `isProd` is **truthy**:

```scss
body {

}
```

### // $$.onlyProd

```scss
body {
  color: gray; // $$.onlyProd
}
```

If `isProd` is **falsy**:

```scss
body {

}
```

If `isProd` is **truthy**:

```scss
body {
  color: gray;
}
```

### // $$.onlyDevStart ... // $$.onlyDevEnd
### /* $$.onlyDevStart ... $$.onlyDevEnd */

```scss
// $$.onlyDevStart
.foo {
  color: gray;
}
// $$.onlyDevEnd
/* $$.onlyDevStart
.foo {
  color: gray;
}
$$.onlyDevEnd */
/* $$.onlyDevStart */
.foo {
  color: gray;
}
/* $$.onlyDevEnd */
```

If `isProd` is **falsy**:

```scss

.foo {
  color: gray;
}


.foo {
  color: gray;
}


.foo {
  color: gray;
}

```

If `isProd` is **truthy**:

```scss
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
```


### // $$.onlyProdStart ... // $$.onlyProdEnd
### /* $$.onlyProdStart ... $$.onlyProdEnd */

```scss
// $$.onlyProdStart
.foo {
  color: gray;
}
// $$.onlyProdEnd
/* $$.onlyProdStart
.foo {
  color: gray;
}
$$.onlyProdEnd */
/* $$.onlyProdStart */
.foo {
  color: gray;
}
/* $$.onlyProdEnd */
```

If `isProd` is **falsy**:

```scss
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
```

If `isProd` is **truthy**:

```scss

.foo {
  color: gray;
}


.foo {
  color: gray;
}


.foo {
  color: gray;
}

```
