# html

*Read this in other languages: [English](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/html.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.html({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (必須)

webpackがプロダクションモードかどうかを表す式を指定してください。

### prefix: `string` (default: `'\\$\\$\\.'`)

置換対象の文字列の接頭辞を指定します。

この文字列は RegExp() に渡されるため、エスケープが必要な文字が存在します。
(例: 接頭辞として`$`を使用したい場合は、`'\\$'`を指定してください。)

つまり、接頭辞のデフォルトは`$$.`です。
以降の説明ではデフォルトの接頭辞を想定します。

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

[string-replace-loader](https://github.com/Va1/string-replace-loader)
に渡すoptionsの配列を指定できます。

## Replacements

### <!-- $$.onlyDevStart --> ... <!-- $$.onlyDevEnd -->
### <!-- $$.onlyDevStart ... $$.onlyDevEnd -->

```html
<!-- $$.onlyDevStart -->
<a href="./debug.html">debug page</a>
<!-- $$.onlyDevEnd -->
<!-- $$.onlyDevStart
<a href="./debug.html">debug page</a>
$$.onlyDevEnd -->
```

If `isProd` is **falsy**:

```html

<a href="./debug.html">debug page</a>


<a href="./debug.html">debug page</a>

```

If `isProd` is **truthy**:

```html
<!--
<a href="./debug.html">debug page</a>
-->
<!--
<a href="./debug.html">debug page</a>
-->
```

### <!-- $$.onlyProdStart --> ... <!-- $$.onlyProdEnd -->
### <!-- $$.onlyProdStart ... $$.onlyProdEnd -->

```html
<!-- $$.onlyProdStart -->
production mode
<!-- $$.onlyProdEnd -->
<!-- $$.onlyProdStart
production mode
$$.onlyProdEnd -->
```

If `isProd` is **falsy**:

```html
<!--
production mode
-->
<!--
production mode
-->
```

If `isProd` is **truthy**:

```html

production mode


production mode

```
