# js

*Read this in other languages: [English](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/js.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.js$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.js({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (必須)

webpackがプロダクションモードかどうかを表す式を指定してください。

### prefix: `string` (default: `'\\$\\$\\.'`)

置換対象の文字列の接頭辞を指定します。

この文字列は RegExp() に渡されるため、エスケープが必要な文字が存在します。
(例: 接頭辞として`$`を使用したい場合は、`'\\$'`を指定してください。)

つまり、接頭辞のデフォルトは`$$.`です。
以降の説明ではデフォルトの接頭辞を想定します。

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

[string-replace-loader](https://github.com/Va1/string-replace-loader)
に渡すoptionsの配列を指定できます。

## Replacements

### // $$.onlyDev

```javascript
let a = 1;
console.log(a); // $$.onlyDev
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;
console.log(a);
a++;
```

If `isProd` is **truthy**:

```javascript
let a = 1;

a++;
```

### // $$.onlyProd

```javascript
let a = 1;
console.log(a); // $$.onlyProd
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;

a++;
```

If `isProd` is **truthy**:

```javascript
let a = 1;
console.log(a);
a++;
```

### // $$.onlyDevStart ... // $$.onlyDevEnd
### /* $$.onlyDevStart ... $$.onlyDevEnd */

```javascript
// $$.onlyDevStart
let a = 1;
console.log(a);
a++;
// $$.onlyDevEnd
/* $$.onlyDevStart
let b = 1;
console.log(b);
b++;
$$.onlyDevEnd */
```

If `isProd` is **falsy**:

```javascript

let a = 1;
console.log(a);
a++;


let b = 1;
console.log(b);
b++;

```

If `isProd` is **truthy**:

```javascript
/*
let a = 1;
console.log(a);
a++;
*/
/*
let b = 1;
console.log(b);
b++;
*/
```


### // $$.onlyProdStart ... // $$.onlyProdEnd
### /* $$.onlyProdStart ... $$.onlyProdEnd */

```javascript
// $$.onlyProdStart
let a = 1;
console.log(a);
a++;
// $$.onlyProdEnd
/* $$.onlyProdStart
let b = 1;
console.log(b);
b++;
$$.onlyProdEnd */
```

If `isProd` is **falsy**:

```javascript
/*
let a = 1;
console.log(a);
a++;
*/
/*
let b = 1;
console.log(b);
b++;
*/
```

If `isProd` is **truthy**:

```javascript

let a = 1;
console.log(a);
a++;


let b = 1;
console.log(b);
b++;

```

### $$.debug(`exp`);

* `exp`に Template literal(\`で文字列を囲むリテラル記法) は使用できません。

* lintツールを使用している場合は、`$$`をグローバル変数として認識させる必要があります。

```javascript
let a = 1;
$$.debug(a);
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;
console.log(`a`);console.log(a);
a++;
```

If `isProd` is **truthy**:

```javascript

let a = 1;

a++;

```

### $$.startTimer(); ... $$.stopTimer();

* lintツールを使用している場合は、`$$`をグローバル変数として認識させる必要があります。

* 接頭辞に関わらず、生成される変数名は`$$startTime`です。

```javascript
$$.startTimer();
let a = 1;
console.log(a);
a++;
$$.stopTimer();
```

If `isProd` is **falsy**:

```javascript
var $$startTime = performance.now();
let a = 1;
console.log(a);
a++;
console.log(performance.now()-$$startTime);
```

If `isProd` is **truthy**:

```javascript

let a = 1;
console.log(a);
a++;

```
