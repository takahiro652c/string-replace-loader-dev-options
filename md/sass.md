# sass

*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/sass.ja.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.sass$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.sass({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (required)

### prefix: `string` (default: `'\\$\\$\\.'`)

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

## Replacements

### // $$.onlyDev

```scss
body {
  color: gray; // $$.onlyDev
}
```

If `isProd` is **falsy**:

```scss
body {
  color: gray;
}
```

If `isProd` is **truthy**:

```scss
body {

}
```

### // $$.onlyProd

```scss
body {
  color: gray; // $$.onlyProd
}
```

If `isProd` is **falsy**:

```scss
body {

}
```

If `isProd` is **truthy**:

```scss
body {
  color: gray;
}
```

### // $$.onlyDevStart ... // $$.onlyDevEnd
### /* $$.onlyDevStart ... $$.onlyDevEnd */

```scss
// $$.onlyDevStart
.foo {
  color: gray;
}
// $$.onlyDevEnd
/* $$.onlyDevStart
.foo {
  color: gray;
}
$$.onlyDevEnd */
/* $$.onlyDevStart */
.foo {
  color: gray;
}
/* $$.onlyDevEnd */
```

If `isProd` is **falsy**:

```scss

.foo {
  color: gray;
}


.foo {
  color: gray;
}


.foo {
  color: gray;
}

```

If `isProd` is **truthy**:

```scss
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
```


### // $$.onlyProdStart ... // $$.onlyProdEnd
### /* $$.onlyProdStart ... $$.onlyProdEnd */

```scss
// $$.onlyProdStart
.foo {
  color: gray;
}
// $$.onlyProdEnd
/* $$.onlyProdStart
.foo {
  color: gray;
}
$$.onlyProdEnd */
/* $$.onlyProdStart */
.foo {
  color: gray;
}
/* $$.onlyProdEnd */
```

If `isProd` is **falsy**:

```scss
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
/*
.foo {
  color: gray;
}
*/
```

If `isProd` is **truthy**:

```scss

.foo {
  color: gray;
}


.foo {
  color: gray;
}


.foo {
  color: gray;
}

```
