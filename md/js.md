# js

*Read this in other languages: [日本語](https://gitlab.com/takahiro652c/string-replace-loader-dev-options/blob/master/md/js.ja.md)*

```javascript
var stringReplaceLoaderDevOptions = require('string-replace-loader-dev-options');
module.exports = (env, argv) => {
  var isProd = argv.mode === 'production';
  return {
    // ...
    module: {
      rules: [
        {
          test: /\.js$/,
          use: [
            // other loaders
            // ...
            stringReplaceLoaderDevOptions.js({isProd: isProd})
          ]
        }
      ]
    }
  };
}
```

## Options

### isProd: `boolean` (required)

### prefix: `string` (default: `'\\$\\$\\.'`)

### others: `{search: string, replace: string, flags?: string}[]` (default: `null`)

## Replacements

### // $$.onlyDev

```javascript
let a = 1;
console.log(a); // $$.onlyDev
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;
console.log(a);
a++;
```

If `isProd` is **truthy**:

```javascript
let a = 1;

a++;
```

### // $$.onlyProd

```javascript
let a = 1;
console.log(a); // $$.onlyProd
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;

a++;
```

If `isProd` is **truthy**:

```javascript
let a = 1;
console.log(a);
a++;
```

### // $$.onlyDevStart ... // $$.onlyDevEnd
### /* $$.onlyDevStart ... $$.onlyDevEnd */

```javascript
// $$.onlyDevStart
let a = 1;
console.log(a);
a++;
// $$.onlyDevEnd
/* $$.onlyDevStart
let b = 1;
console.log(b);
b++;
$$.onlyDevEnd */
```

If `isProd` is **falsy**:

```javascript

let a = 1;
console.log(a);
a++;


let b = 1;
console.log(b);
b++;

```

If `isProd` is **truthy**:

```javascript
/*
let a = 1;
console.log(a);
a++;
*/
/*
let b = 1;
console.log(b);
b++;
*/
```


### // $$.onlyProdStart ... // $$.onlyProdEnd
### /* $$.onlyProdStart ... $$.onlyProdEnd */

```javascript
// $$.onlyProdStart
let a = 1;
console.log(a);
a++;
// $$.onlyProdEnd
/* $$.onlyProdStart
let b = 1;
console.log(b);
b++;
$$.onlyProdEnd */
```

If `isProd` is **falsy**:

```javascript
/*
let a = 1;
console.log(a);
a++;
*/
/*
let b = 1;
console.log(b);
b++;
*/
```

If `isProd` is **truthy**:

```javascript

let a = 1;
console.log(a);
a++;


let b = 1;
console.log(b);
b++;

```

### $$.debug(`exp`);

* Can't use "Template literal" (\`) in exp.

```javascript
let a = 1;
$$.debug(a);
a++;
```

If `isProd` is **falsy**:

```javascript
let a = 1;
console.log(`a`);console.log(a);
a++;
```

If `isProd` is **truthy**:

```javascript

let a = 1;

a++;

```

### $$.startTimer(); ... $$.stopTimer();

```javascript
$$.startTimer();
let a = 1;
console.log(a);
a++;
$$.stopTimer();
```

If `isProd` is **falsy**:

```javascript
var $$startTime = performance.now();
let a = 1;
console.log(a);
a++;
console.log(performance.now()-$$startTime);
```

If `isProd` is **truthy**:

```javascript

let a = 1;
console.log(a);
a++;

```
