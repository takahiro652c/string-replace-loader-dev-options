import { getSassMultiple } from './sass';
import {
  getOnlyDevStart,
  getOnlyProdStart,
  getOnlyDevEnd,
  getOnlyProdEnd,
  getOnlyDev,
  getOnlyProd
} from './js';

const prefix = '\\$\\$\\.';

test('test getSassMultiple', () => {
  let loader;
  loader = getSassMultiple({ isProd: true });
  expect(loader).toContainEqual(getOnlyDevStart(true, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefix));
  expect(loader).toContainEqual(getOnlyDev(true, prefix));
  expect(loader).toContainEqual(getOnlyProd(true, prefix));
  loader = getSassMultiple({ isProd: false });
  expect(loader).toContainEqual(getOnlyDevStart(false, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefix));
  expect(loader).toContainEqual(getOnlyDev(false, prefix));
  expect(loader).toContainEqual(getOnlyProd(false, prefix));
  const prefixA = 'A';
  loader = getSassMultiple({ isProd: true, prefix: prefixA });
  expect(loader).toContainEqual(getOnlyDevStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefixA));
  expect(loader).toContainEqual(getOnlyDev(true, prefixA));
  expect(loader).toContainEqual(getOnlyProd(true, prefixA));
  loader = getSassMultiple({ isProd: false, prefix: prefixA });
  expect(loader).toContainEqual(getOnlyDevStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefixA));
  expect(loader).toContainEqual(getOnlyDev(false, prefixA));
  expect(loader).toContainEqual(getOnlyProd(false, prefixA));
});
