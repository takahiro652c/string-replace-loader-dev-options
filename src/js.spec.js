// TODO: いろいろなテスト追加

import {
  getDebug,
  getStartTimer,
  getStopTimer,
  getOnlyDevStart,
  getOnlyProdStart,
  getOnlyDevEnd,
  getOnlyProdEnd,
  getOnlyDev,
  getOnlyProd,
  getJsMultiple
} from './js';

const prefix = '\\$\\$\\.';

test('test $$.debug(exp);', () => {
  let _;
  const testString = `
$$.debug(a);
$$.debug(a);
`;
  _ = getDebug(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getDebug(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
console.log(\`a\`);console.log(a);
console.log(\`a\`);console.log(a);
`);
});

test('test $$.startTimer();', () => {
  let _;
  const testString = `
$$.startTimer();
$$.startTimer();
`;
  _ = getStartTimer(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getStartTimer(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
var $$startTime = performance.now();
var $$startTime = performance.now();
`);
});

test('test $$.stopTimer();', () => {
  let _;
  const testString = `
$$.stopTimer();
$$.stopTimer();
`;
  _ = getStopTimer(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getStopTimer(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
console.log(performance.now()-$$startTime);
console.log(performance.now()-$$startTime);
`);
});

test('test // $$.onlyDevStart', () => {
  let _;
  const testString = `
// $$.onlyDevStart
// $$.onlyDevStart
`;
  _ = getOnlyDevStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
/*
/*
`);
  _ = getOnlyDevStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test /* $$.onlyDevStart', () => {
  let _;
  const testString = `
/* $$.onlyDevStart
/* $$.onlyDevStart
/* $$.onlyDevStart */
/* $$.onlyDevStart */
`;
  _ = getOnlyDevStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
/*
/*
/*
/*
`);
  _ = getOnlyDevStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`




`);
});

test('test // $$.onlyProdStart', () => {
  let _;
  const testString = `
// $$.onlyProdStart
// $$.onlyProdStart
`;
  _ = getOnlyProdStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
/*
/*
`);
});

test('test /* $$.onlyProdStart', () => {
  let _;
  const testString = `
/* $$.onlyProdStart
/* $$.onlyProdStart
/* $$.onlyProdStart */
/* $$.onlyProdStart */
`;
  _ = getOnlyProdStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`




`);
  _ = getOnlyProdStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
/*
/*
/*
/*
`);
});

test('test // $$.onlyDevEnd', () => {
  let _;
  const testString = `
// $$.onlyDevEnd
// $$.onlyDevEnd
`;
  _ = getOnlyDevEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
*/
*/
`);
  _ = getOnlyDevEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test $$.onlyDevEnd */', () => {
  let _;
  const testString = `
$$.onlyDevEnd */
$$.onlyDevEnd */
/* $$.onlyDevEnd */
/* $$.onlyDevEnd */
`;
  _ = getOnlyDevEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
*/
*/
*/
*/
`);
  _ = getOnlyDevEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`




`);
});

test('test // $$.onlyProdEnd', () => {
  let _;
  const testString = `
// $$.onlyProdEnd
// $$.onlyProdEnd
`;
  _ = getOnlyProdEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
*/
*/
`);
});

test('test $$.onlyProdEnd */', () => {
  let _;
  const testString = `
$$.onlyProdEnd */
$$.onlyProdEnd */
/* $$.onlyProdEnd */
/* $$.onlyProdEnd */
`;
  _ = getOnlyProdEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`




`);
  _ = getOnlyProdEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
*/
*/
*/
*/
`);
});

test('test // $$.onlyDev', () => {
  let _;
  const testString = `
hoge// $$.onlyDev
fuga// $$.onlyDev
`;
  _ = getOnlyDev(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyDev(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
hoge
fuga
`);
});

test('test // $$.onlyProd', () => {
  let _;
  const testString = `
hoge// $$.onlyProd
fuga// $$.onlyProd
`;
  _ = getOnlyProd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
hoge
fuga
`);
  _ = getOnlyProd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test getJsMultiple', () => {
  let loader;
  loader = getJsMultiple({ isProd: true });
  expect(loader).toContainEqual(getDebug(true, prefix));
  expect(loader).toContainEqual(getStartTimer(true, prefix));
  expect(loader).toContainEqual(getStopTimer(true, prefix));
  expect(loader).toContainEqual(getOnlyDevStart(true, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefix));
  expect(loader).toContainEqual(getOnlyDev(true, prefix));
  expect(loader).toContainEqual(getOnlyProd(true, prefix));
  loader = getJsMultiple({ isProd: false });
  expect(loader).toContainEqual(getDebug(false, prefix));
  expect(loader).toContainEqual(getStartTimer(false, prefix));
  expect(loader).toContainEqual(getStopTimer(false, prefix));
  expect(loader).toContainEqual(getOnlyDevStart(false, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefix));
  expect(loader).toContainEqual(getOnlyDev(false, prefix));
  expect(loader).toContainEqual(getOnlyProd(false, prefix));
  const prefixA = 'A';
  loader = getJsMultiple({ isProd: true, prefix: prefixA });
  expect(loader).toContainEqual(getDebug(true, prefixA));
  expect(loader).toContainEqual(getStartTimer(true, prefixA));
  expect(loader).toContainEqual(getStopTimer(true, prefixA));
  expect(loader).toContainEqual(getOnlyDevStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefixA));
  expect(loader).toContainEqual(getOnlyDev(true, prefixA));
  expect(loader).toContainEqual(getOnlyProd(true, prefixA));
  loader = getJsMultiple({ isProd: false, prefix: prefixA });
  expect(loader).toContainEqual(getDebug(false, prefixA));
  expect(loader).toContainEqual(getStartTimer(false, prefixA));
  expect(loader).toContainEqual(getStopTimer(false, prefixA));
  expect(loader).toContainEqual(getOnlyDevStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefixA));
  expect(loader).toContainEqual(getOnlyDev(false, prefixA));
  expect(loader).toContainEqual(getOnlyProd(false, prefixA));
});
