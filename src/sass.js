import {
  getOnlyDevStart,
  getOnlyProdStart,
  getOnlyDevEnd,
  getOnlyProdEnd,
  getOnlyDev,
  getOnlyProd
} from './js';

export const getSassMultiple = ({ isProd, prefix = '\\$\\$\\.' }) => {
  const $$ = {};
  $$.onlyDevStart = getOnlyDevStart(isProd, prefix);
  $$.onlyProdStart = getOnlyProdStart(isProd, prefix);
  $$.onlyDevEnd = getOnlyDevEnd(isProd, prefix);
  $$.onlyProdEnd = getOnlyProdEnd(isProd, prefix);
  $$.onlyDev = getOnlyDev(isProd, prefix);
  $$.onlyProd = getOnlyProd(isProd, prefix);

  const multiple = [
    $$.onlyDevStart,
    $$.onlyProdStart,
    $$.onlyDevEnd,
    $$.onlyProdEnd,
    $$.onlyDev,
    $$.onlyProd
  ];
  return multiple;
};
