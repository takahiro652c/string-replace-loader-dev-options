import { getJsMultiple } from './js';
import { getHtmlMultiple } from './html';
import { getSassMultiple } from './sass';

function checkOptions(options) {
  if (options == null || options.isProd == null) {
    throw new Error('"isProd" is required');
  }
}

function exportLoader(array, { others }) {
  const loader = { loader: 'string-replace-loader', options: {} };
  const multiple = [];
  if (Array.isArray(others)) {
    multiple.push(...others);
  }
  if (Array.isArray(array)) {
    multiple.push(...array);
  }
  if (multiple.length > 1) {
    loader.options.multiple = multiple;
  } else {
    loader.options = multiple[0];
  }
  return loader;
}

export const js = (options = {}) => {
  checkOptions(options);
  return exportLoader(getJsMultiple(options), options);
};
export const html = (options = {}) => {
  checkOptions(options);
  return exportLoader(getHtmlMultiple(options), options);
};
export const sass = (options = {}) => {
  checkOptions(options);
  return exportLoader(getSassMultiple(options), options);
};
