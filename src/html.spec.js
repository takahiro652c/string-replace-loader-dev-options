// TODO: いろいろなテスト追加

import {
  getOnlyDevStart,
  getOnlyProdStart,
  getOnlyDevEnd,
  getOnlyProdEnd,
  getHtmlMultiple
} from './html';

const prefix = '\\$\\$\\.';

test('test <!-- $$.onlyDevStart -->', () => {
  let _;
  const testString = `
<!-- $$.onlyDevStart -->
<!-- $$.onlyDevStart -->
`;
  _ = getOnlyDevStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
<!--
<!--
`);
  _ = getOnlyDevStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test <!-- $$.onlyDevStart', () => {
  let _;
  const testString = `
<!-- $$.onlyDevStart
<!-- $$.onlyDevStart
`;
  _ = getOnlyDevStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
<!--
<!--
`);
  _ = getOnlyDevStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test <!-- $$.onlyProdStart -->', () => {
  let _;
  const testString = `
<!-- $$.onlyProdStart -->
<!-- $$.onlyProdStart -->
`;
  _ = getOnlyProdStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
<!--
<!--
`);
});

test('test <!-- $$.onlyProdStart', () => {
  let _;
  const testString = `
<!-- $$.onlyProdStart
<!-- $$.onlyProdStart
`;
  _ = getOnlyProdStart(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdStart(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
<!--
<!--
`);
});

test('test <!-- $$.onlyDevEnd -->', () => {
  let _;
  const testString = `
<!-- $$.onlyDevEnd -->
<!-- $$.onlyDevEnd -->
`;
  _ = getOnlyDevEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
-->
-->
`);
  _ = getOnlyDevEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test $$.onlyDevEnd -->', () => {
  let _;
  const testString = `
$$.onlyDevEnd -->
$$.onlyDevEnd -->
`;
  _ = getOnlyDevEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
-->
-->
`);
  _ = getOnlyDevEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
});

test('test <!-- $$.onlyProdEnd -->', () => {
  let _;
  const testString = `
<!-- $$.onlyProdEnd -->
<!-- $$.onlyProdEnd -->
`;
  _ = getOnlyProdEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
-->
-->
`);
});

test('test $$.onlyProdEnd -->', () => {
  let _;
  const testString = `
$$.onlyProdEnd -->
$$.onlyProdEnd -->
`;
  _ = getOnlyProdEnd(true, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`


`);
  _ = getOnlyProdEnd(false, prefix);
  expect(testString.replace(RegExp(_.search, _.flags), _.replace)).toEqual(`
-->
-->
`);
});

test('test getJsMultiple', () => {
  let loader;
  loader = getHtmlMultiple({ isProd: true });
  expect(loader).toContainEqual(getOnlyDevStart(true, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefix));
  loader = getHtmlMultiple({ isProd: false });
  expect(loader).toContainEqual(getOnlyDevStart(false, prefix));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefix));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefix));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefix));
  const prefixA = 'A';
  loader = getHtmlMultiple({ isProd: true, prefix: prefixA });
  expect(loader).toContainEqual(getOnlyDevStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(true, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(true, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(true, prefixA));
  loader = getHtmlMultiple({ isProd: false, prefix: prefixA });
  expect(loader).toContainEqual(getOnlyDevStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdStart(false, prefixA));
  expect(loader).toContainEqual(getOnlyDevEnd(false, prefixA));
  expect(loader).toContainEqual(getOnlyProdEnd(false, prefixA));
});
