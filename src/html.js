export const getHtmlMultiple = ({ isProd, prefix = '\\$\\$\\.' }) => {
  const $$ = {};
  $$.onlyDevStart = getOnlyDevStart(isProd, prefix);
  $$.onlyProdStart = getOnlyProdStart(isProd, prefix);
  $$.onlyDevEnd = getOnlyDevEnd(isProd, prefix);
  $$.onlyProdEnd = getOnlyProdEnd(isProd, prefix);

  const multiple = [
    $$.onlyDevStart,
    $$.onlyProdStart,
    $$.onlyDevEnd,
    $$.onlyProdEnd
  ];
  return multiple;
};

// Usage: <!-- $$.onlyDevStart(?: -->)?
export function getOnlyDevStart(isProd, prefix) {
  return {
    search: `<!-- ${prefix}onlyDevStart(?: -->)?`,
    replace: isProd ? '<!--' : '',
    flags: 'g'
  };
}
// Usage: <!-- $$.onlyProdStart(?: -->)?
export function getOnlyProdStart(isProd, prefix) {
  return {
    search: `<!-- ${prefix}onlyProdStart(?: -->)?`,
    replace: isProd ? '' : '<!--',
    flags: 'g'
  };
}
// Usage: (?:<!-- )?$$.onlyDevEnd -->
export function getOnlyDevEnd(isProd, prefix) {
  return {
    search: `(?:<!-- )?${prefix}onlyDevEnd -->`,
    replace: isProd ? '-->' : '',
    flags: 'g'
  };
}
// Usage: (?:<!-- )?$$.onlyProdEnd -->
export function getOnlyProdEnd(isProd, prefix) {
  return {
    search: `(?:<!-- )?${prefix}onlyProdEnd -->`,
    replace: isProd ? '' : '-->',
    flags: 'g'
  };
}
