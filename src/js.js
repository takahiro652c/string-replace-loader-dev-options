export const getJsMultiple = ({ isProd, prefix = '\\$\\$\\.' }) => {
  const $$ = {};
  $$.debug = getDebug(isProd, prefix);
  $$.startTimer = getStartTimer(isProd, prefix);
  $$.stopTimer = getStopTimer(isProd, prefix);
  $$.onlyDevStart = getOnlyDevStart(isProd, prefix);
  $$.onlyProdStart = getOnlyProdStart(isProd, prefix);
  $$.onlyDevEnd = getOnlyDevEnd(isProd, prefix);
  $$.onlyProdEnd = getOnlyProdEnd(isProd, prefix);
  $$.onlyDev = getOnlyDev(isProd, prefix);
  $$.onlyProd = getOnlyProd(isProd, prefix);

  const multiple = [
    $$.debug,
    $$.startTimer,
    $$.stopTimer,
    $$.onlyDevStart,
    $$.onlyProdStart,
    $$.onlyDevEnd,
    $$.onlyProdEnd,
    $$.onlyDev,
    $$.onlyProd
  ];
  return multiple;
};

// Usage: $$.debug(exp);
export function getDebug(isProd, prefix) {
  return {
    search: `${prefix}debug\\((.+)\\);`,
    replace: isProd ? '' : 'console.log(`$1`);console.log($1);',
    flags: 'g'
  };
}
// Usage: $$.startTimer();
export function getStartTimer(isProd, prefix) {
  return {
    search: `${prefix}startTimer\\(\\);`,
    replace: isProd ? '' : 'var $$$$startTime = performance.now();',
    flags: 'g'
  };
}
// Usage: $$.stopTimer();
export function getStopTimer(isProd, prefix) {
  return {
    search: `${prefix}stopTimer\\(\\);`,
    replace: isProd ? '' : 'console.log(performance.now()-$$$$startTime);',
    flags: 'g'
  };
}
// Usage: // $$.onlyDevStart, /* $$.onlyDevStart */, /* $$.onlyDevStart
export function getOnlyDevStart(isProd, prefix) {
  const key = `${prefix}onlyDevStart`;
  return {
    search: `// ${key}|/\\* ${key}(?: \\*/)?`,
    replace: isProd ? '/*' : '',
    flags: 'g'
  };
}
// Usage: // $$.onlyProdStart, /* $$.onlyProdStart */, /* $$.onlyProdStart
export function getOnlyProdStart(isProd, prefix) {
  const key = `${prefix}onlyProdStart`;
  return {
    search: `// ${key}|/\\* ${key}(?: \\*/)?`,
    replace: isProd ? '' : '/*',
    flags: 'g'
  };
}
// Usage: // $$.onlyDevEnd, /* $$.onlyDevEnd */, $$.onlyDevEnd */
export function getOnlyDevEnd(isProd, prefix) {
  const key = `${prefix}onlyDevEnd`;
  return {
    search: `// ${key}|(?:/\\* )?${key} \\*/`,
    replace: isProd ? '*/' : '',
    flags: 'g'
  };
}
// Usage: // $$.onlyProdEnd, /* $$.onlyProdEnd */, $$.onlyProdEnd */
export function getOnlyProdEnd(isProd, prefix) {
  const key = `${prefix}onlyProdEnd`;
  return {
    search: `// ${key}|(?:/\\* )?${key} \\*/`,
    replace: isProd ? '' : '*/',
    flags: 'g'
  };
}
// Usage: exp // $$.onlyDev
export function getOnlyDev(isProd, prefix) {
  return {
    search: `^(.*)// ${prefix}onlyDev`,
    replace: isProd ? '' : '$1',
    flags: 'gm'
  };
}
// Usage: exp // onlyProd
export function getOnlyProd(isProd, prefix) {
  return {
    search: `^(.*)// ${prefix}onlyProd`,
    replace: isProd ? '$1' : '',
    flags: 'gm'
  };
}
